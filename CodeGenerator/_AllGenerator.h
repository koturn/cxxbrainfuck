#ifndef GENERATORS_H
#define GENERATORS_H


#include "Lang/GeneratorC.h"
#include "Lang/GeneratorCpp.h"
#include "Lang/GeneratorCSharp.h"
#include "Lang/GeneratorJava.h"
#include "Lang/GeneratorLua.h"
#include "Lang/GeneratorPython.h"
#include "Lang/GeneratorRuby.h"



#endif  // GENERATORS_H
